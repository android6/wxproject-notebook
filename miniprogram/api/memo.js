/**
 * 获取备忘录列表
 */
function getMemoList(pageNum, pageSize) {
  //获取数据库的对象
  const db = wx.cloud.database()
  //获取到备忘录的数据库集合数据
  console.log(db.collection('memo').get())
  // if(str){
  //   memo = memo.where({
  //     title:db.RegExp({
  //       regexp:str,//从搜索栏中获取的value作为规则进行匹配。
  //       options: 'i', //不区分大小写
  //     })
  //   })
  // }
  return db.collection('memo').skip(pageNum).limit(pageSize).orderBy('order', 'desc').get()
}
/**
 * 获取总条目数
 */
function getMemoListCount() {
  const db = wx.cloud.database()
  return new Promise((resolve, reject) => {
    db.collection('memo').count().then(res => {
      resolve(res.total)
    })
  })
}

/**
 * 获取备忘录详情
 */
function getMemoInfo(id) {
  const db = wx.cloud.database()
  return db.collection('memo').doc(id).get()
}
/**
 * 新增备忘录
 */
function addMemoInfo(data) {
  const db = wx.cloud.database()
  return db.collection('memo').add({ data })
}
/**
 * 删除备忘录
 */
function deleteMemoInfo(id) {
  const db = wx.cloud.database()
  return db.collection('memo').doc(id).remove()
}
/**
 * 修改备忘录
 */
function updateMemoInfo(id, data) {
  const db = wx.cloud.database()
  return db.collection('memo').doc(id).update({ data })
}
/**
 * 获取本地列表
 */
function getLocalMemoList() {
  return wx.getStorageSync('itemDataKey') || []
}

export {
  getMemoList,
  getMemoInfo,
  deleteMemoInfo,
  addMemoInfo,
  updateMemoInfo,
  getLocalMemoList,
  getMemoListCount
}