function getBackground(pageNum, pageSize) {
  const db = wx.cloud.database()
  const background = db.collection('background')
  //获取集合数据
  return background.skip(pageNum).limit(pageSize).orderBy('order', 'desc').get()
}
function addBackground(data) {
  const db = wx.cloud.database()
  //添加创建时间
  data.createTime = db.serverDate()
  return db.collection('background').add({ data })
}
/**
 * 
 * @param {*} id :背景列表的id
 * @param {*} fielId : 图片的id
 */
function deleteBackground(id, fileID) {
  const db = wx.cloud.database()
  // 删除云存储背景图片
  wx.cloud.deleteFile({
    fileList: [fileID]
  })
  // 删除背景数据
  return db.collection('background').doc(id).remove()
}
/**
 * 修改背景列表
 * @param {*} id 
 */
function updateBackground(id) {
  const db = wx.cloud.database()
  let data = {
    order: new Date().getTime()
  }
  return db.collection('background').doc(id).update({ data })
}

/**
 * 获取总条目数
 */
function getBackgroundCount() {
  const db = wx.cloud.database()
  return db.collection('background').count()
}

export {
  getBackground,
  addBackground,
  deleteBackground,
  updateBackground,
  getBackgroundCount,
}