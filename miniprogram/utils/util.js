const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}
// 获取当前日期
function getNowDate() {
  let dateTime
  let YYYY = new Date().getFullYear()
  let MM = new Date().getMonth() + 1
  let DD = new Date().getDate()
  dateTime = YYYY + '-' + MM + '-' + DD
  return dateTime
}

// 获取当前时间
function getNowTime(e) {
  let dateTime
  let HH = new Date().getHours()
  let mm = new Date().getMinutes() < 10 ? '0' + new Date().getMinutes() :
    new Date().getMinutes()

  dateTime = HH + ':' + mm
  return dateTime
}

function dataHandle(data) {
  //将追加后的数组再次存储到Storage
  try {
    wx.setStorageSync('itemDataKey', data.list)
  } catch (error) {

  }
}

module.exports = {
  formatTime,
  getNowDate,
  getNowTime,
  dataHandle
}
