// pages/index/index.js
import {
  addMemoInfo,
  getLocalMemoList
} from '../../api/memo'
Page({
  /**
   * 跳转到列表页面
   */
  toList() {
    //添加本地测试数据
    // let list = wx.getStorageSync('itemDataKey') || []
    // const itemData = {
    //   title: '标题1',
    //   content: '内容1',
    //   date: '2021-07-27 23:47',
    //   order: '165844112',
    // }
    // list.unshift(itemData)
    // wx.setStorageSync('itemDataKey', list)
    wx.navigateTo({
      url: '../list/list',
    })
  },

  /**
   * 页面的初始数据
   */
  data: {
    img: {
      normal: '/images/logo_btn.png',
      press: '/images/logo_btn_press.png'
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      imgUrl: this.data.img.normal
    })
    this.judgeLocalMemoList()
  },

  onViewClick() {
  },

  onBtnLogoPress() {
    this.setData({
      imgUrl: this.data.img.press
    })
  },

  onBtnLogoNormal() {
    this.setData({
      imgUrl: this.data.img.normal
    })
  },
  /**
   * 判断本地数据列表
   */
  judgeLocalMemoList() {
    let dataList = getLocalMemoList()
    if (dataList.length > 0) {
      wx.showModal({
        title: '提示',
        content: '检测到本地有数据,是否同步?',
        cancelText: '取消',
        confirmText: '同步',
        success(res) {
          if (res.confirm) {
            dataList.forEach(function (item, index) {
              //遍历列表添加到云存储
              const itemData = {
                title: item.title,
                content: item.content,
                date: item.date,
                order: item.order,
              }
              addMemoInfo(itemData)
            })
            dataList = []
            wx.setStorageSync('itemDataKey', dataList)
          }
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})