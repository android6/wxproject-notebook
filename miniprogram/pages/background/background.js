// miniprogram/pages/background/background.js

import {
  getBackground,
  deleteBackground,
  updateBackground,
  getBackgroundCount,
} from '../../api/background.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    slideButtons: [
      { type: 'warn', text: '删除' }
    ],
    backgroundList: [],
    pageNum: 0,
    pageSize: 5
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getBackground()
  },

  /**
   * 获取背景图列表
   */
  getBackground() {
    getBackground(this.data.pageNum, this.data.pageSize).then(res => {
      this.setData({
        backgroundList: res.data
      })
      console.log(this.data.backgroundList)
    })
  },
  /**
   * 加载背景图列表
   */
  loadBackground() {
    getBackgroundCount().then(res => {
      if (this.data.backgroundList.length < res.total) {
        wx.showLoading({
          title: '正在加载中',
        })
        getBackground(this.data.pageNum, this.data.pageSize).then(res => {
          this.pageNum++
          this.setData({
            backgroundList: this.data.backgroundList.concat(res.data)
          })
          wx.hideLoading({
            success: (res) => {},
          })
          console.log(this.data.backgroundList)
        })
      } else {
        wx.showToast({
          title: '没有更多数据',
          icon: 'error'
        })
      }
    })
  },
  /**
   * 选择图片
   * @param {} e 
   */
  selectBackground(e) {
    wx.showModal({
      title: '提示',
      content: '确定使用该图片作为首页背景图吗?',
      success(res) {
        if (res.confirm) {
          //把选中的排到第一个
          updateBackground(e.currentTarget.dataset.id).then(res => {
            wx.navigateBack({})
          })
        }
      }
    })
  },

  /**
   * slideview内部按钮点击
   * @param {*} e 
   */
  slideviewClick: function (e) {
    if (e.detail.index == 0) {
      let id = e.currentTarget.dataset.id
      let fileID = e.currentTarget.dataset.fileid
      deleteBackground(id, fileID).then(res => {
        this.pageNum--;
        this.getBackground()
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.loadBackground()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})