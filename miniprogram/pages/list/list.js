// pages/list/list.js
const app = getApp()
import {
  getMemoList,
  getMemoInfo,
  deleteMemoInfo,
  updateMemoInfo,
  getMemoListCount,
} from '../../api/memo.js'
import {
  addBackground,
  getBackground,
  deleteBackground
} from '../../api/background.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    chatMessage: ", 你好吗?",
    listData: [],
    pageNum: 0,//从第0页开始取
    pageSize: 5,//每次取5条
    topImgFileId: '',
  },
  /**
   * 选择图片
   */
  chooseImg: function () {
    let that = this
    wx.chooseImage({
      count: 1,
      sourceType: ['album', 'camera'],
      sizeType: ['original', 'compressed'],
      success(res) {
        const filePath = res.tempFilePaths[0]
        //云存储命名规则
        const cloudPath = `top-img/${Date.now()}-${Math.floor(Math.random(0, 1) * 100000)}` + filePath.match(/.[^.]+?$/)[0]
        wx.cloud.uploadFile({
          cloudPath,
          filePath,
          success: res => {
            console.log('上传成功后获得的res: ', res)
            that.setData({
              topImgFileId: res.fileID
            })
            let data = {
              fileID: res.fileID,
              order: new Date().getTime()
            }
            addBackground(data)
            if (that.data.background) {
              //如果已经上传过背景图片,就删除上一个上传的列表数据和存储的图片
              deleteBackground(that.background._id, that.background.fileID)
            }
          },
          fail: res => {
            console.log('上传失败: ', res)
          }
        })
      }
    })
  },
  /**
   * 监听输入框
   * @param {*} e 
   */
  searchInput: function (e) {
    getMemoList(e.detail.value).then(res => {
      this.refreshListData(res.data)
    })
  },

  delete(e) {
    let that = this
    let id = e.currentTarget.dataset.id
    let title = e.currentTarget.dataset.title
    wx.showModal({
      title: '提示',
      content: '确定要删除 ' + title + ' 吗?',
      cancelText: '点错了',
      cancelColor: '#FF0000',
      confirmText: '当然',
      success(res) {
        if (res.confirm) {
          deleteMemoInfo(id).then(res => {
            getMemoList(that.data.pageNum, that.data.pageSize).then(res => {
              that.refreshListData(res.data)
              wx.showToast({
                title: '删除成功',
              })
            })
          })
        } else if (res.cancel) {
          wx.showToast({
            title: '下次注意,别点错了',
            icon: 'none'
          })
        }
      }
    })
  },
  changeText() {
    let msg = app.changeText()
    this.setData({
      chatMessage: msg
    })
  },

  touchStart(e) {
    let dataList = [...this.data.listData]
    dataList.forEach(item => {
      //让列表所有的滑动块隐藏
      if (item.isTouchMove) {
        item.isTouchMove = !item.isTouchMove
      }
    });
    //打印起始的触摸X轴位置

    this.setData({
      listData: dataList,
      startX: e.touches[0].clientX,
      startY: e.touches[0].clientY
    })
  },

  touchMove(e) {
    let moveX = e.changedTouches[0].clientX
    let moveY = e.changedTouches[0].clientY
    let indexs = e.currentTarget.dataset.index
    let dataList = [...this.data.listData]
    let angle = this.angle({
      X: this.data.startX,
      X: this.data.startY,
    }, {
      X: moveX,
      Y: moveY
    })
    dataList.forEach((item, index) => {
      //设为默认不移动
      item.isTouchMove = false
      if (angle > 30) {
        return
      }
      if (indexs == index) {
        //如果滑动的是当前下标
        if (moveX > this.data.startX) {
          //右滑
          item.isTouchMove = false
        } else {
          //左滑
          item.isTouchMove = true
        }
      }
    })
    this.setData({
      listData: dataList
    })
  },
  /**
   * 计算滑动角度
   * @param {*} start 起点坐标
   * @param {*} end 终点坐标
   */
  angle: function (start, end) {
    var _X = end.X - start.startX
    var _Y = end.Y - start.startY
    return 360 * Math.atan(_Y / _X) / (2 * Math.PI)
  },

  toEditPage(e) {
    let id = e.currentTarget.dataset.id
    if (id) {
      let data = e.currentTarget.dataset
      wx.navigateTo({
        url: '../edit/edit?id=' + data.id + '&title=' + data.title + '&content=' + data.content,
      })
    } else {
      wx.navigateTo({
        url: '../edit/edit',
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      isEmpty: !this.data.listData.length > 0,
      slideButtons: [{
        text: '置顶',
      }, {
        type: 'warn',
        text: '删除',
      }],
    })
  },
  /**
   * 获取背景图片
   */
  getBackground() {
    getBackground(0, 5).then(res => {
      if (res.data.length) {
        this.setData({
          background: res.data[0],
          topImgFileId: res.data[0].fileID
        })
      }
    })
  },

  slideButtonTap: function (e) {
    if (e.detail.index == 0) {
      //置顶
      this.setTop(e)
    } else if (e.detail.index == 1) {
      //删除
      this.delete(e)
    }
  },

  setTop: function (e) {
    let id = e.currentTarget.dataset.id
    let data = {
      isTop: true,
      order: new Date().getTime()
    }
    updateMemoInfo(id, data)
    getMemoList(0, (this.data.pageNum == 0 ? 1 : this.data.pageNum) * this.data.pageSize).then(res => {
      this.refreshListData(res.data)
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
    * 生命周期函数--监听页面显示
    */
  onShow: function () {
    getMemoList(this.data.pageNum, this.data.pageSize).then(res => {
      this.refreshListData(res.data)
    })
    this.getBackground()
  },
  toBackgroundList() {
    wx.navigateTo({
      url: '../background/background',
    })
  },

  refreshListData(listData) {
    this.setData({
      listData: listData,
      isEmpty: !listData.length > 0
    })
  },
  /**
   * 分页加载优化
   */
  async loadListData() {
    //获取数据库的总条目数
    let count = await getMemoListCount()
    //判断是否全部获取完
    if (this.data.listData.length < count) {
      console.log("+++")
      //当前列表数据小于数据库的数量
      //先显示一个loading
      wx.showLoading({
        title: '数据加载中',
      })
      //当前页+1
      this.data.pageNum++
      //获取下一页数据
      getMemoList(this.data.pageNum, this.data.pageSize).then(res => {
        this.setData({
          listData: this.data.listData.concat(res.data)
        })
        wx.hideLoading()
      })
    } else {
      wx.showToast({
        title: '没有更多数据',
        icon: 'error'
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.loadListData()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})