// pages/edit/edit.js
import {
  dataHandle
} from '../../utils/util.js'
import {
  addMemoInfo,
  getMemoInfo,
  updateMemoInfo
} from '../../api/memo.js'
const dayjs = require('dayjs')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    inputTitle: '',
    inputContent: '',
    index: -1,
    utils: require('../../utils/util'),
    //跳转类型 1.添加(默认) 2.修改
    type: 1,
    list: [],
    a: [{
      id: '1',
      name: '5',
      price: 'Coupon',
      show: true,
    }, {
      id: '2',
      name: '10',
      price: 'Coupon',
      show: false,
    }]
  },
  setInputTitle: function (strTitle) {
    this.setData({ inputTitle: strTitle.detail.value })
  },
  setInputContent: function (strContent) {
    this.setData({ inputContent: strContent.detail.value })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //如果index下标不为空,说明是修改
      this.setData({
        currentTime: this.data.utils.formatTime(new Date()).replaceAll('/', '-')
      })
    if (options.id) {
      this.setData({
        type: 2,
      })
      getMemoInfo(options.id).then(res => {
        let data = res.data
        this.setData({
          id: options.id,
          inputTitle: data.title,
          inputContent: data.content,
          currentTime: data.date
        })
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //保存按钮
  saveContent: function () {
    if (!this.data.inputTitle) {
      wx.showToast({
        title: '请输入标题',
        icon: 'error'
      })
      return
    }
    if (!this.data.inputContent) {
      wx.showToast({
        title: '请输入内容',
        icon: 'error'
      })
      return
    }
    if (this.data.inputTitle.length < 3) {
      wx.showToast({
        title: '标题长度不能少于3个',
        icon: 'none'
      })
      return
    }
    if (this.data.inputContent.length < 5) {
      wx.showToast({
        title: '内容长度不能少于5个',
        icon: 'none'
      })
      return
    }
    //先将单个的条目信息赋值到itemData
    let itemData = {
      title: this.data.inputTitle,
      content: this.data.inputContent,
      date: this.data.utils.formatTime(new Date()).replaceAll('/', '-'),
      order: new Date().getTime()
    }
    if (this.data.type == 1) {
      addMemoInfo(itemData).then(res => {
        wx.navigateBack({})
      })
    } else {
      updateMemoInfo(this.data.id, itemData).then(res => {
        wx.navigateBack({})
      })
    }
    wx.showToast({
      title: this.data.type == 1 ? '添加成功' : '修改成功',
      icon: 'success',
      duration: 2000
    })
  },
  getNowTime() {
    return dayjs().format('YYYY-MM-DD HH:mm')
  }
})