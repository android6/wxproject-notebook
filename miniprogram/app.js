// app.js
App({
  "cloud": true,
  onLaunch() {
    //初始化云存储,让项目可以使用云存储
    wx.cloud.init()
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
  },
  globalData: {
    userInfo: null,
    chatMsgArray: [',你来了', '想你很久了', '快来记录', '你最近好吗', '你好吗?']
  },
  changeText() {
    let index = Math.floor((Math.random() * this.globalData.chatMsgArray.length))
    let data = this.globalData.chatMsgArray[index]
    return data
  },
})
